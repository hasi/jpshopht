import request from '@/utils/request'

//员工管理

/**
 * 获取员工列表
 * @param {*} params 
 */
export function getEmpList(params) {
  return request({
    url: '/merchantSubUser',
    method: 'get',
    params
  })
}
/**
 * 获取员工
 * @param {*} params 
 */
export function getEmp(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantSubUser/'+id,
    method: 'get',
    params
  })
}
/**
 * 新增员工
 * @param {*} params 
 */
export function postEmp(params) {
  return request({
    url: '/merchantSubUser',
    method: 'post',
    data: params
  })
}

/**
 * 修改员工
 * @param {*} params 
 */
export function putEmp(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantSubUser/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除员工
 * @param {*} params 
 */
export function delEmp(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantSubUser/'+id,
    method: 'delete',
    data: params
  })
}


//角色

/**
 * 获取角色列表
 * @param {*} params 
 */
export function getRoleList(params) {
  return request({
    url: '/merchantSubGroup',
    method: 'get',
    params
  })
}
/**
 * 获取功能列表
 * @param {*} params 
 */
export function getActionList(params) {
  return request({
    url: '/merchantNewRules',
    method: 'get',
    params
  })
}
/**
 * 修改角色
 * @param {*} params 
 */
export function putRole(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantSubGroup/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除角色
 * @param {*} params 
 */
export function delRole(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantSubGroup/'+id,
    method: 'delete',
    data: params
  })
}
/**
 * 新增角色
 * @param {*} params 
 */
export function postRole(params) {
  return request({
    url: '/merchantSubGroup',
    method: 'post',
    data: params
  })
}


//客服管理

/**
 * 获取客服列表
 * @param {*} params 
 */
export function getKeList(params) {
  return request({
    url: '/merchantSubKefu',
    method: 'get',
    params
  })
}